package com.example.mplayer;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.gauravk.audiovisualizer.visualizer.BarVisualizer;

import java.io.File;
import java.util.ArrayList;

public class playerActivity<function> extends AppCompatActivity {
    public static final String EXTRA_NAME = "song_name";
    static MediaPlayer mediaPlayer;
    Button btnplay, btnprev, btnnext, btnfastforward, btnrewind;
    TextView txtsongname, txtsongstart, txtsongend;
    SeekBar seekBar;
    BarVisualizer barVisualizer;
    ImageView imageView;
    String songName;
    int position;
    ArrayList<File> mySongs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        btnprev = findViewById(R.id.btnprevious);
        btnfastforward = findViewById(R.id.btnforward);
        btnrewind = findViewById(R.id.btnrewind);
        btnnext = findViewById(R.id.btnNext);
        btnplay = findViewById(R.id.btnplay);

        txtsongname = findViewById(R.id.forsongname);
        txtsongstart = findViewById(R.id.txtsongStart);
        txtsongend = findViewById(R.id.txtsongEnd);

        seekBar = findViewById(R.id.seekbar);
        barVisualizer = findViewById(R.id.wave);
        imageView = findViewById(R.id.imgview);

        if (mediaPlayer != null) {
            mediaPlayer.start();

            mediaPlayer.release();

        }

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        mySongs = (ArrayList) bundle.getParcelableArrayList("songs");
        String sName = intent.getStringExtra("Songname");

        position = bundle.getInt("pos", 0);
        txtsongname.setSelected(true);
        Uri uri = Uri.parse(mySongs.get(position).toString());
        songName = mySongs.get(position).getName();
        txtsongname.setText(songName);

        mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
        mediaPlayer.start();

        btnplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer.isPlaying()) {
                    visulizer();
                    btnplay.setBackgroundResource(R.drawable.play);
                    mediaPlayer.pause();
                } else {
                    btnplay.setBackgroundResource(R.drawable.pause);

                    mediaPlayer.start();
                    visulizer();

                }
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                btnnext.performClick();


            }
        });

        btnnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                mediaPlayer.release();
                position = ((position + 1) % mySongs.size());
                Uri uri = Uri.parse(mySongs.get(position).toString());
                songName = mySongs.get(position).getName();
                txtsongname.setText(songName);
                mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
                btnplay.setBackgroundResource(R.drawable.pause);
                mediaPlayer.start();
                visulizer();
            }
        });

        btnprev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                mediaPlayer.release();
                position = ((position - 1) < 0) ? (mySongs.size() - 1) : position - 1;
                Uri uri = Uri.parse(mySongs.get(position).toString());
                songName = mySongs.get(position).getName();
                txtsongname.setText(songName);
                mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);

                visulizer();
                btnplay.setBackgroundResource(R.drawable.pause);

                mediaPlayer.start();
            }
        });


    }

    public void visulizer() {
        int audiosessionId = mediaPlayer.getAudioSessionId();
        if (audiosessionId != -1) {
            barVisualizer.setAudioSessionId(audiosessionId);
        }
    }


}
