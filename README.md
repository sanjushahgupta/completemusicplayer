Through this music player, you can access/listen all songs present in your phone's external memory storage.

Alluring user interface with loaded animations and music bars.

Play your songs on the go without even having an internet connection.

Play, pause, rewind,back, forward your music.

Listen to music even on your device background—no need to keep the app open all the time.

Music Visualizer that helps you to visualize the beats of a song.


